const codeWrapper = {
    init() {
        const formEl = document.querySelector('form');

        formEl.addEventListener('submit', codeWrapper.handleFormSubmit)
    },
    handleFormSubmit: (evt) => {
        evt.preventDefault();

        const { value: formerCodeValue } = evt.target['former-code'];

        if (codeWrapper.isCodeValid(formerCodeValue)) {
            codeWrapper.displayNewCode(formerCodeValue);
        } else {
            codeWrapper.displayWarning();
        }
    },
    displayNewCode: (formerCode) => {
        const newCode = codeWrapper.generateNewCode(formerCode);

        const contentToAdd = `Le nouveau code est : ${newCode}`;
        codeWrapper.addTextElementToBody(contentToAdd);
    },
    displayWarning: () => {
        const warning = 'Attention ! Le code entrée n\'est pas valide !';
        codeWrapper.addTextElementToBody(warning);
    },
    addTextElementToBody(content) {
        const pEl = document.querySelector('#new-code');

        if (!pEl) {
            const newPEl = document.createElement('p');
            newPEl.id = 'new-code';
            newPEl.textContent = content;

            const body = document.querySelector('body');

            body.appendChild(newPEl);
        } else {
            pEl.textContent = content;
        }
    },
    generateNewCode(formerCode) {
        const figures = formerCode.split('').map((figure) => parseInt(figure, 10));

        return figures.map((figure, index) => {
            let newFigure;
            if ([0, 2, 4].includes(index)) {
                newFigure = figure - 1;

                if (newFigure === -1) {
                    newFigure = 9;
                }
            } else {
                newFigure = figure + 1;

                if (newFigure === 10) {
                    newFigure = 0;
                }
            }

            return newFigure;
        }).join('');
    },
    isCodeValid (value) {
        return !!value.match(/^[0-9]{5}$/)
    }
}

document.addEventListener('DOMContentLoaded', codeWrapper.init);
